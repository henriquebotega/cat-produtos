import uuid from 'uuid'

import { base } from '../firebase'

const INITIAL_STATE = {
    data: {}
}

export default function crud(state = INITIAL_STATE, action) {

    switch (action.type) {
        case "INSERIR":
            base.push(action.entidade, {
                data: { ...action.data, id: uuid() }
            })
            return { ...state }

        case "EDITAR":
            base.update(action.entidade + '/' + action.id, {
                data: { ...action.data }
            })
            return { ...state }

        case "EXCLUIR":
            base.remove(action.entidade + '/' + action.id)

            // Remove da coleção atual
            const colStateData = { ...state.data }
            delete colStateData[action.id]

            return { data: colStateData }

        case "LOAD":
            return { data: action.data || {} }

        default:
            return state
    }

}