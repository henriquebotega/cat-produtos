import React, { Component } from 'react';

import { Route, BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store'

import Home from './components/Home'
import Header from './components/Header'

import ListProdutos from './components/produtos/Listar'
import EditProdutos from './components/produtos/Editar'

import EditServicos from './components/servicos/Editar'
import ListServicos from './components/servicos/Listar'

import { Container } from 'semantic-ui-react'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Container>
            <Header />

            <Route path='/' component={Home} exact />
            <Route path='/produtos' component={ListProdutos} exact />
            <Route path='/produtos/:id' component={EditProdutos} />
            <Route path='/servicos' component={ListServicos} exact />
            <Route path='/servicos/:id' component={EditServicos} />
          </Container>
        </Router>
      </Provider>
    );
  }
}

export default App;
