import Rebase from 're-base'
import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyAwbgKyrQry413Os6IfcdTos4SHv8aZwQc",
    authDomain: "cat-produtos.firebaseapp.com",
    databaseURL: "https://cat-produtos.firebaseio.com",
    projectId: "cat-produtos",
    storageBucket: "",
    messagingSenderId: "321535433168"
};

const app = firebase.initializeApp(config);

export const database = app.database()
export const base = Rebase.createClass(database)