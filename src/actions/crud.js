import { database } from '../firebase'

export const adicionar = (entidade, data) => {
    return {
        type: 'INSERIR',
        entidade: entidade,
        data: data
    }
}

export const editar = (entidade, data, id) => {
    return {
        type: 'EDITAR',
        entidade: entidade,
        data: data,
        id: id
    }
}

export const remover = (entidade, id) => {
    return {
        type: 'EXCLUIR',
        entidade: entidade,
        id: id
    }
}

export const load = (data) => {
    return {
        type: 'LOAD',
        data
    }
}

export const loadData = (entidade) => {
    return async (dispatch) => {
        database.ref(entidade).once('value', snapshot => {
            dispatch(load(snapshot.val()))
        })
    }
}
