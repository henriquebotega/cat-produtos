import React, { Component } from 'react';

import { Image } from 'semantic-ui-react'

class Home extends Component {
    render() {
        return <Image src='/home.jpg' size='medium' centered />
    }
}

export default Home