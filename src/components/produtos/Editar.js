import React, { Component } from 'react';

import { adicionar, editar } from '../../actions/crud'
import { connect } from 'react-redux'

import { base } from '../../firebase'
import { Button, Form, Dimmer, Loader, Message } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'

class EditProdutos extends Component {
    state = {
        titulo: '',
        descricao: '',
        isLoading: false,
        redirect: false
    }

    componentDidMount() {
        this.setState({ isLoading: true })
        const id = this.props.match.params['id']

        if (id && id !== '0') {
            base.fetch('produtos/' + id, {
                context: this,
                isArray: false,
                then(ret) {
                    this.setState({ titulo: ret['titulo'], descricao: ret['descricao'] })
                    this.setState({ isLoading: false })
                }
            })
        } else {
            this.setState({ isLoading: false })
        }
    }

    handleState = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSalvar = () => {
        const id = this.props.match.params['id']

        if (this.state.titulo !== '') {

            const obj = {
                titulo: this.state.titulo,
                descricao: this.state.descricao
            }

            if (id && id !== '0') {
                this.props.editar(obj, id)
            } else {
                this.props.adicionar(obj)
            }

            this.setState({ titulo: '', descricao: '' })
            this.setState({ redirect: true })
        }
    }

    render() {

        if (this.state.redirect) {
            return <Redirect to='/produtos' />
        }

        return (
            <div>
                <Dimmer active={this.state.isLoading}>
                    <Loader indeterminate>Carregando...</Loader>
                </Dimmer>

                {!this.state.isLoading &&
                    <Form warning={(this.state.titulo === '')}>
                        <Form.Field>
                            <label>Titulo <span style={{color: '#ff3366'}}>*</span></label>
                            <input type='text' value={this.state.titulo} name='titulo' onChange={this.handleState} /> <br />
                        </Form.Field>

                        <Form.Field>
                            <label>Descrição</label>
                            <textarea value={this.state.descricao} name='descricao' onChange={this.handleState} /> <br />
                        </Form.Field>

                        <Form.Field>
                            <Message warning header='Aviso!' list={['Preencha os campos solicitados corretamente!']} />
                        </Form.Field>

                        <Form.Field>
                            <Button basic color='grey' onClick={() => this.setState({ redirect: true })}>Cancelar</Button>
                            <Button basic color={(this.state.titulo === '') ? 'grey' : 'green'} onClick={() => this.handleSalvar()}>Salvar</Button>
                        </Form.Field>
                    </Form>
                }
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        adicionar: (obj) => dispatch(adicionar('produtos', obj)),
        editar: (obj, id) => dispatch(editar('produtos', obj, id))
    }
}

export default connect(null, mapDispatchToProps)(EditProdutos)