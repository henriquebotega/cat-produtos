import React, { Component } from 'react';

import { loadData, remover } from '../../actions/crud'
import { connect } from 'react-redux'

import { database } from '../../firebase'
import { Dimmer, Loader, Table, Icon, Input, Form, Button, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import SweetAlert from 'sweetalert2-react';

class ListProdutos extends Component {

    state = {
        isLoading: false,
        registros: {},
        showSweetAlert: false,
        objAtual: {},
        loadingPalavraChave: false,
        palavraChave: ''
    }

    componentDidMount() {
        this.setState({ isLoading: true })
        this.props.load()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.items !== this.props.items) {
            this.setState({ registros: nextProps.items })
            this.setState({ isLoading: false })
        }
    }

    confirmarExcluir = (objAtual) => {
        this.setState({
            showSweetAlert: true,
            objAtual
        })
    }

    handleExcluir = () => {
        this.props.remover(this.state.objAtual)

        this.setState({
            showSweetAlert: false,
            objAtual: {}
        })
    }

    renderLI = (itemAtual, objAtual, i) => {
        return (
            <Table.Row key={i}>
                <Table.Cell>
                    {itemAtual.titulo}
                </Table.Cell>
                <Table.Cell>
                    {itemAtual.descricao}
                </Table.Cell>
                <Table.Cell>
                    <Button basic color='brown' as={Link} to={`/produtos/${objAtual}`}>Editar</Button>
                    <Button basic color='red' onClick={() => this.confirmarExcluir(objAtual)}>Excluir</Button>
                </Table.Cell>
            </Table.Row>
        )
    }

    handleState = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    limparFiltro = () => {
        this.setState({
            palavraChave: ''
        })

        this.buscarPalavraChave();
    }

    buscarPalavraChave = () => {
        this.setState({ loadingPalavraChave: true })

        setTimeout(() => {
            const palavraChave = this.state.palavraChave

            database.ref("produtos").orderByValue().on("value", snapshot => {
                let colFiltrados = {}

                snapshot.forEach(function (data) {
                    const item = data.val()

                    if (item['titulo'].indexOf(palavraChave) > -1 || item['descricao'].indexOf(palavraChave) > -1) {
                        colFiltrados[data.key] = data.val()
                    }

                });

                this.setState({ registros: colFiltrados })
                this.setState({ loadingPalavraChave: false })
            });

        }, 1000)
    }

    render() {
        const colRegistros = Object.keys(this.state.registros)

        return (
            <div>
                <SweetAlert show={this.state.showSweetAlert} title="Atenção" type='warning' text="Deseja realmente excluir este registro?"
                    showCancelButton={true} confirmButtonText='Sim' cancelButtonText='Não' onConfirm={() => this.handleExcluir()} />

                <h2>Produtos</h2>

                <Dimmer active={this.state.isLoading}>
                    <Loader indeterminate>Carregando...</Loader>
                </Dimmer>

                {!this.state.isLoading &&
                    <Form>
                        <Form.Field>
                            <Button basic as={Link} to='/produtos/0'>Novo</Button>
                        </Form.Field>

                        <Form.Field>
                            <Input loading={this.state.loadingPalavraChave} placeholder='Informe uma palavra-chave...' icon={<Icon name='close' onClick={() => this.limparFiltro()} inverted circular link />}
                                value={this.state.palavraChave} name='palavraChave' onChange={this.handleState} onKeyDown={this.buscarPalavraChave} />
                        </Form.Field>
                    </Form>
                }

                {!this.state.isLoading && colRegistros.length === 0 &&
                    <Segment color='teal'>Nenhum registro encontrado</Segment>
                }

                {!this.state.isLoading && colRegistros.length > 0 &&
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Titulo</Table.HeaderCell>
                                <Table.HeaderCell>Descrição</Table.HeaderCell>
                                <Table.HeaderCell>Ações</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {colRegistros.map((obj, i) => this.renderLI(this.state.registros[obj], obj, i))}
                        </Table.Body>
                    </Table>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.crud.data
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        load: () => dispatch(loadData('produtos')),
        remover: (id) => dispatch(remover('produtos', id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListProdutos)