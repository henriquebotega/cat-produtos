import React, { Component } from 'react';

import { Link } from 'react-router-dom'

import { Menu, Image } from 'semantic-ui-react'

class Header extends Component {
    render() {
        return (
            <Menu>
                <Menu.Item as={Link} to='/'>
                    <Image src='/bag.svg' size='small'  />
                </Menu.Item>

                <Menu.Item as={Link} to='/produtos'>Produtos</Menu.Item>
                <Menu.Item as={Link} to='/servicos'>Serviços</Menu.Item>
            </Menu>
        )
    }
}

export default Header